package com.example.gustavo.fafica;

import android.content.Intent;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button cadButton = findViewById(R.id.Cadbutton);
        cadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar(v);
            }
        });

        Button listaButton = findViewById(R.id.Listbutton);
        listaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listar(v);
            }
        });
    }

    private void cadastrar(View view) {
        Intent intent = new Intent(this, CadActivity.class);
        startActivity(intent);
    }

    private void listar(View view) {
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }
}
