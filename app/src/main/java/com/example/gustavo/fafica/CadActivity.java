package com.example.gustavo.fafica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gustavo.fafica.Repo.Pessoa;
import com.example.gustavo.fafica.Repo.PessoaRepo;

public class CadActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.fafica.gustavo.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad);

        Button bCadastrar = findViewById(R.id.button2);
        bCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView nome = findViewById(R.id.editText);
                TextView sobrenome = findViewById(R.id.editText2);
                RadioGroup rg = findViewById(R.id.radiogroup);
                String rchecked = null;

                int checked = rg.getCheckedRadioButtonId();
                if (checked != -1) {
                    RadioButton rb = (RadioButton) findViewById(checked);
                    rchecked = rb.getText().toString();
                }

                Pessoa novaPessoa = new Pessoa(nome.getText().toString(), sobrenome.getText().toString(), rchecked);

                if (cadastrar(novaPessoa)) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra(EXTRA_MESSAGE, novaPessoa.getNome());
                    Toast toast = Toast.makeText(getApplicationContext(), novaPessoa.getNome(), Toast.LENGTH_LONG);
                    toast.show();
                    startActivity(intent);
                }
            }
        });
    }

    private boolean cadastrar(Pessoa novaPessoa) {
        PessoaRepo.p.add(novaPessoa);
        return true;
    }
}
