package com.example.gustavo.fafica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.gustavo.fafica.Repo.Pessoa;
import com.example.gustavo.fafica.Repo.PessoaRepo;

import java.io.Serializable;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final ListView visualizar = (ListView) findViewById(R.id.cadastros);
        ArrayAdapter<Pessoa> adapter = new ArrayAdapter<Pessoa>(this, android.R.layout.simple_list_item_1, PessoaRepo.p);
        visualizar.setAdapter(adapter);

        visualizar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa p = (Pessoa) visualizar.getAdapter().getItem(position);
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("nome", p.getNome());
                intent.putExtra("sobrenome", p.getSobrenome());
                intent.putExtra("sexo", p.getSexo());
                startActivity(intent);
            }
        });
    }
}
