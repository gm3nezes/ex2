package com.example.gustavo.fafica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        TextView tv = (TextView) findViewById(R.id.textView);

        tv.setText("DETALHES:\n" + "NOME: " +
                intent.getStringExtra("nome") + "\nSOBRENOME: " + intent.getStringExtra("sobrenome")
        + "\nSEXO: " + intent.getStringExtra("sexo"));
    }
}
